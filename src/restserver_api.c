/*
 * restserver - A smart, efficient, low consumption RESTful application service platform writen in C
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the Apache License v2.0, see the file LICENSE in base directory.
 */

#include "in.h"

struct HttpEnv *RSAPIGetHttpEnv( struct RestServerContext *ctx )
{
	return ctx->p_accepted_session->http_env;
}

char *RSAPIGetHttpMethodPtr( struct RestServerContext *ctx , int *p_method_len )
{
	return GetRestServerContextHttpMethod( ctx , p_method_len );
}

char *RSAPIGetHttpUriPtr( struct RestServerContext *ctx , int *p_uri_len )
{
	return GetRestServerContextHttpUri( ctx , p_uri_len );
}

int RSAPIGetHttpUriPathsCount( struct RestServerContext *ctx )
{
	return GetRestServerContextHttpUriPathsCount( ctx );
}

char *RSAPIGetHttpUriPathPtr( struct RestServerContext *ctx , int index , int *p_path_len )
{
	return GetRestServerContextHttpUriPathPtr( ctx , index , p_path_len );
}

int RSAPIGetHttpUriQueriesCount( struct RestServerContext *ctx )
{
	return GetRestServerContextHttpUriQueriesCount( ctx );
}

char *RSAPIGetHttpUriQueryKeyPtr( struct RestServerContext *ctx , int index , int *p_key_len )
{
	return GetRestServerContextHttpUriQueryKeyPtr( ctx , index , p_key_len );
}

char *RSAPIGetHttpUriQueryValuePtr( struct RestServerContext *ctx , int index , int *p_value_len )
{
	return GetRestServerContextHttpUriQueryValuePtr( ctx , index , p_value_len );
}

char *RSAPIGetHttpRequestBodyPtr( struct RestServerContext *ctx , int *p_body_len )
{
	return GetRestServerContextHttpRequestBodyPtr( ctx , p_body_len );
}

void RSAPISetUserData( struct RestServerContext *ctx , void *user_data )
{
	SetRestServerContextUserData( ctx , user_data );
	return;
}

void *RSAPIGetUserData( struct RestServerContext *ctx )
{
	return GetRestServerContextUserData( ctx );
}

int RSAPIFormatHttpResponse( struct RestServerContext *ctx , char *http_response_body , int http_response_body_len , char *http_header_format , ... )
{
	va_list			valist ;
	
	int			nret = 0 ;
	
	va_start( valist , http_header_format );
	nret = FormatResetServerContextHttpResponse( ctx , http_response_body , http_response_body_len , http_header_format , valist ) ;
	va_end( valist );
	return nret;
}

struct RestServiceControler *RSAPICreateRestServiceControler( struct RestServiceConfig *config_array )
{
	return CreateRestServiceControler( config_array );
}

int RSAPIDispatchRestServiceControler( struct RestServiceControler *ctl , struct RestServerContext *ctx )
{
	return DispatchRestServiceControler( ctl , ctx );
}

void RSAPIDestroyRestServiceControler( struct RestServiceControler *ctl )
{
	return DestroyRestServiceControler( ctl );
}

