/*
 * restserver - A smart, efficient, low consumption RESTful application service platform writen in C
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the Apache License v2.0, see the file LICENSE in base directory.
 */

#include "in.h"

char	__RESTSERVER_VERSION_0_7_0[] = "0.7.0" ;
char	*__RESTSERVER_VERSION = __RESTSERVER_VERSION_0_7_0 ;

extern int tcpmain( struct TcpdaemonServerEnvironment *p_env , int sock , void *p_addr );

int main( int argc , char *argv[] )
{
	char				conf_pathfilename[ PATH_MAX ] ;
	char				*file_content = NULL ;
	struct RestServerContext	ctx ;
	
	int				nret = 0 ;
	
	struct TcpdaemonEntryParameter	tcpdaemon_entry_para ;
	
	if( argc == 1 + 1 )
	{
		memset( conf_pathfilename , 0x00 , sizeof(conf_pathfilename) );
		strncpy( conf_pathfilename , argv[1] , sizeof(conf_pathfilename)-1 );
	}
	else
	{
		printf( "restserver v%s\n" , __RESTSERVER_VERSION );
		printf( "copyright by calvin<calvinwilliams@163.com><calvinwilliams.c@gmail.com>\n" );
		printf( "USAGE : restserver config_pathfile\n" );
		exit(9);
	}
	
	file_content = StrdupEntireFile( conf_pathfilename , NULL ) ;
	if( file_content == NULL )
	{
		printf( "*** ERROR : config file[%s] not found\n" , conf_pathfilename );
		exit(1);
	}
	
	memset( & ctx , 0x00 , sizeof(struct RestServerContext) );
	nret = DSCDESERIALIZE_JSON_restserver_conf( NULL , file_content , NULL , & (ctx.conf) ) ;
	if( nret )
	{
		printf( "*** ERROR : config file[%s] content invalid\n" , conf_pathfilename );
		free( file_content );
		exit(1);
	}
	
	free( file_content );
	
	nret = ExpandPathFilename( ctx.conf.log.log_pathfilename , sizeof(ctx.conf.log.log_pathfilename) ) ;
	if( nret )
	{
		printf( "*** ERROR : expand log_pathfilename in config failed[%d]\n" , nret );
		exit(1);
	}
	
	nret = ExpandPathFilename( ctx.conf.server.application_so_pathfilename , sizeof(ctx.conf.server.application_so_pathfilename) ) ;
	if( nret )
	{
		printf( "*** ERROR : expand application_so_pathfilename in config failed[%d]\n" , nret );
		exit(1);
	}
	
	memset( & tcpdaemon_entry_para , 0x00 , sizeof(struct TcpdaemonEntryParameter) );
	strncpy( tcpdaemon_entry_para.log_pathfilename , ctx.conf.log.log_pathfilename , sizeof(tcpdaemon_entry_para.log_pathfilename)-1 );
	tcpdaemon_entry_para.log_level = LOGLEVEL_FATAL ;
	if( STRCMP( ctx.conf.log.log_level , == , "DEBUG" ) )
		tcpdaemon_entry_para.log_level = LOGLEVEL_DEBUG ;
	else if( STRCMP( ctx.conf.log.log_level , == , "INFO" ) )
		tcpdaemon_entry_para.log_level = LOGLEVEL_INFO ;
	else if( STRCMP( ctx.conf.log.log_level , == , "WARN" ) )
		tcpdaemon_entry_para.log_level = LOGLEVEL_WARN ;
	else if( STRCMP( ctx.conf.log.log_level , == , "ERROR" ) )
		tcpdaemon_entry_para.log_level = LOGLEVEL_ERROR ;
	else if( STRCMP( ctx.conf.log.log_level , == , "FATAL" ) )
		tcpdaemon_entry_para.log_level = LOGLEVEL_FATAL ;
	strcpy( tcpdaemon_entry_para.server_model , "IOMP" );
	tcpdaemon_entry_para.timeout_seconds = ctx.conf.http.timeout_seconds ;
	strncpy( tcpdaemon_entry_para.ip , ctx.conf.http.listen_ip , sizeof(tcpdaemon_entry_para.ip)-1 );
	tcpdaemon_entry_para.port = ctx.conf.http.listen_port ;
	tcpdaemon_entry_para.process_count = ctx.conf.server.workers_count ;
	tcpdaemon_entry_para.pfunc_tcpmain = & tcpmain ;
	tcpdaemon_entry_para.param_tcpmain = (void*) & ctx ;
	
	return -tcpdaemon( & tcpdaemon_entry_para );
}

