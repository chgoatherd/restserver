/*
 * restserver - A smart, efficient, low consumption RESTful application service platform writen in C
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the Apache License v2.0, see the file LICENSE in base directory.
 */

#include "in.h"

_WINDLL_FUNC int tcpmain( struct TcpdaemonServerEnvironment *tcpdaemon_env , int sock , void *p_addr )
{
	struct AcceptedSession		*p_accepted_session = NULL ;
	struct HttpEnv			*http = NULL ;
	struct RestServerContext	*ctx = NULL ;
	struct HttpHeader		*header = NULL ;
	struct HttpBuffer		*buf = NULL ;
	char				*p = NULL ;
	int				len ;
	
	int				nret = 0 ;
	
	ctx = (struct RestServerContext *)TDGetTcpmainParameter( tcpdaemon_env ) ;
	
	switch( TDGetIoMultiplexEvent(tcpdaemon_env) )
	{
		/* 接受新连接事件 */
		case IOMP_ON_ACCEPTING_SOCKET :
			INFOLOG( "tcpmain IOMP_ON_ACCEPTING_SOCKET" )
			
			/* 申请内存以存放已连接会话 */
			p_accepted_session = (struct AcceptedSession *)malloc( sizeof(struct AcceptedSession) ) ;
			if( p_accepted_session == NULL )
				return TCPMAIN_RETURN_ERROR;
			memset( p_accepted_session , 0x00 , sizeof(struct AcceptedSession) );
			
			p_accepted_session->sock = sock ;
			memcpy( & (p_accepted_session->addr) , p_addr , sizeof(struct sockaddr) );
			p_accepted_session->http_env = CreateHttpEnv() ;
			if( ctx->conf.http.timeout_seconds > 0 )
			{
				SetHttpTimeout( p_accepted_session->http_env , ctx->conf.http.timeout_seconds );
				DEBUGLOG( "SetHttpTimeout[%d]" , ctx->conf.http.timeout_seconds )
			}
			if( ctx->conf.http.headers_count_hardmax > 0 )
			{
				SetHttpReceivingHeadersCountHardMax( p_accepted_session->http_env , ctx->conf.http.headers_count_hardmax );
				DEBUGLOG( "SetHttpReceivingHeadersCountHardMax[%d]" , ctx->conf.http.headers_count_hardmax )
			}
			if( ctx->conf.http.headers_len_hardmax > 0 )
			{
				SetHttpReceivingHeadersLenHardMax( p_accepted_session->http_env , ctx->conf.http.headers_len_hardmax );
				DEBUGLOG( "SetHttpReceivingHeadersLenHardMax[%d]" , ctx->conf.http.headers_len_hardmax )
			}
			if( ctx->conf.http.header_content_length_val_hardmax > 0 )
			{
				SetHttpReceivingHeaderContentLengthValHardMax( p_accepted_session->http_env , ctx->conf.http.header_content_length_val_hardmax );
				DEBUGLOG( "SetHttpReceivingHeaderContentLengthValHardMax[%d]" , ctx->conf.http.header_content_length_val_hardmax )
			}
			
			/* 设置已连接会话数据结构 */
			TDSetIoMultiplexDataPtr( tcpdaemon_env , p_accepted_session );
			
			/* 等待读事件 */
			return TCPMAIN_RETURN_WAITINGFOR_RECEIVING;
			
		/* 关闭连接事件 */
		case IOMP_ON_CLOSING_SOCKET :
			INFOLOG( "tcpmain IOMP_ON_CLOSING_SOCKET" )
			
			/* 释放已连接会话 */
			p_accepted_session = (struct AcceptedSession *) p_addr ;
			
			DestroyHttpEnv( p_accepted_session->http_env );
			
			free( p_accepted_session );
			
			/* 注销已连接会话数据结构 */
			TDSetIoMultiplexDataPtr( tcpdaemon_env , NULL );
			
			/* 等待下一任意事件 */
			return TCPMAIN_RETURN_WAITINGFOR_NEXT;
			
		/* 通讯接收事件 */
		case IOMP_ON_RECEIVING_SOCKET :
			INFOLOG( "tcpmain IOMP_ON_RECEIVING_SOCKET" )
			
			p_accepted_session = (struct AcceptedSession *) p_addr ;
			http = p_accepted_session->http_env ;
			
			/* 非堵塞接收通讯数据 */
			nret = ReceiveHttpRequestNonblock( p_accepted_session->sock , NULL , http ) ;
			if( nret == FASTERHTTP_INFO_NEED_MORE_HTTP_BUFFER )
			{
				INFOLOG( "ReceiveHttpRequestNonblock return FASTERHTTP_INFO_NEED_MORE_HTTP_BUFFER" )
				return TCPMAIN_RETURN_WAITINGFOR_NEXT;
			}
			else if( nret == FASTERHTTP_INFO_TCP_CLOSE )
			{
				if( GetHttpBufferLength( GetHttpRequestBuffer(http) ) == 0 )
				{
					INFOLOG( "ReceiveHttpRequestNonblock return FASTERHTTP_INFO_TCP_CLOSE" )
				}
				else
				{
					ERRORLOG( "ReceiveHttpRequestNonblock return FASTERHTTP_INFO_TCP_CLOSE" )
				}
				return TCPMAIN_RETURN_CLOSE;
			}
			else if( nret == FASTERHTTP_ERROR_TCP_CLOSE )
			{
				ERRORLOG( "ReceiveHttpRequestNonblock return FASTERHTTP_ERROR_TCP_CLOSE" )
				return TCPMAIN_RETURN_CLOSE;
			}
			else if( nret )
			{
				ERRORLOG( "ReceiveHttpRequestNonblock return [%d]" , nret )
				return TCPMAIN_RETURN_CLOSE;
			}
			else
			{
				INFOLOG( "ReceiveHttpRequestNonblock ok" )
				
				buf = GetHttpRequestBuffer(http) ;
				p = GetHttpBufferBase( buf , & len ) ;
				INFOLOG( "HTTP REQUEST [%.*s]" , len , p )
				
				INFOLOG( "--- [%.*s] [%.*s] [%.*s] ------------------"
					, GetHttpHeaderLen_METHOD(http) , GetHttpHeaderPtr_METHOD(http,NULL)
					, GetHttpHeaderLen_URI(http) , GetHttpHeaderPtr_URI(http,NULL)
					, GetHttpHeaderLen_VERSION(http) , GetHttpHeaderPtr_VERSION(http,NULL) );
				header = TravelHttpHeaderPtr( http , NULL ) ;
				while( header )
				{
					INFOLOG( "HTTP HREADER [%.*s] [%.*s]" , GetHttpHeaderNameLen(header) , GetHttpHeaderNamePtr(header,NULL) , GetHttpHeaderValueLen(header) , GetHttpHeaderValuePtr(header,NULL) );
					header = TravelHttpHeaderPtr( http , header ) ;
				}
				INFOLOG( "HTTP BODY    [%.*s]" , GetHttpBodyLen(http) , GetHttpBodyPtr(http,NULL) );
				
				ctx->p_accepted_session = p_accepted_session ;
				nret = ProcessHttpRequest( ctx ) ;
				if( nret < 0 )
				{
					FATALLOG( "ProcessHttpRequest failed[%d]" , nret )
					exit(9);
				}
				else if( nret > 0 )
				{
					ERRORLOG( "ProcessHttpRequest failed[%d]" , nret )
					
					nret = FormatHttpResponseStartLine( nret , http , 0 , HTTP_RETURN_NEWLINE ) ;
					if( nret )
					{
						ERRORLOG( "FormatHttpResponseStartLine failed" )
						return TCPMAIN_RETURN_CLOSE;
					}
				}
				else
				{
					INFOLOG( "ProcessHttpRequest ok" )
				}
				
				p = GetHttpBufferBase( GetHttpResponseBuffer(http) , & len ) ;
				INFOLOG( "HTTP RESPONSE [%.*s]" , len , p )
				
				return TCPMAIN_RETURN_WAITINGFOR_SENDING;
			}
			
		/* 通讯发送事件 */
		case IOMP_ON_SENDING_SOCKET :
			p_accepted_session = (struct AcceptedSession *) p_addr ;
			http = p_accepted_session->http_env ;
			
			/* 非堵塞发送通讯数据 */
			nret = SendHttpResponseNonblock( p_accepted_session->sock , NULL , http ) ;
			if( nret == FASTERHTTP_INFO_TCP_SEND_WOULDBLOCK )
			{
				INFOLOG( "SendHttpResponseNonblock return FASTERHTTP_INFO_TCP_SEND_WOULDBLOCK" )
				return TCPMAIN_RETURN_WAITINGFOR_NEXT;
			}
			else if( nret == FASTERHTTP_INFO_TCP_CLOSE )
			{
				INFOLOG( "SendHttpResponseNonblock return FASTERHTTP_INFO_TCP_CLOSE" )
				return TCPMAIN_RETURN_CLOSE;
			}
			else if( nret == FASTERHTTP_ERROR_TCP_CLOSE )
			{
				ERRORLOG( "SendHttpResponseNonblock return FASTERHTTP_ERROR_TCP_CLOSE" )
				return TCPMAIN_RETURN_CLOSE;
			}
			else if( nret )
			{
				ERRORLOG( "SendHttpResponseNonblock return [%d]" , nret )
				return TCPMAIN_RETURN_CLOSE;
			}
			else
			{
				INFOLOG( "SendHttpResponseNonblock ok" )
				
				if( GetHttpStatusCode(http) == HTTP_OK && CheckHttpKeepAlive(http) )
				{
					ResetHttpEnv( http );
					return TCPMAIN_RETURN_WAITINGFOR_RECEIVING;
				}
				else
				{
					return TCPMAIN_RETURN_CLOSE;
				}
			}
			
		default :
			return TCPMAIN_RETURN_ERROR;
	}
}

