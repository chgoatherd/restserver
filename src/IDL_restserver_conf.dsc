STRUCT	restserver_conf
{
	STRUCT	log
	{
		STRING	128	log_pathfilename
		STRING	10	log_level
	}
	
	STRUCT	server
	{
		INT	1	test_mode
		INT	4	workers_count
		STRING	256	application_so_pathfilename
	}
	
	STRUCT	http
	{
		STRING	16	listen_ip
		INT	4	listen_port
		
		STRING	64	domain
		
		INT	4	timeout_seconds
		
		INT	4	headers_count_hardmax
		INT	4	headers_len_hardmax
		INT	4	header_content_length_val_hardmax
	}
}

