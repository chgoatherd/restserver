/*
 * restserver - A smart, efficient, low consumption RESTful application service platform writen in C
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the Apache License v2.0, see the file LICENSE in base directory.
 */

#include "in.h"

/* 读取文件，内容存放到动态申请内存块中 */
/* 注意：使用完后，应用负责释放内存 */
char *StrdupEntireFile( char *pathfilename , int *p_file_len )
{
	char		*file_content = NULL ;
	int		file_len ;
	FILE		*fp = NULL ;
	
	int		nret = 0 ;
	
	fp = fopen( pathfilename , "rb" ) ;
	if( fp == NULL )
	{
		return NULL;
	}
	
	fseek( fp , 0 , SEEK_END );
	file_len  = ftell( fp ) ;
	fseek( fp , 0 , SEEK_SET );
	
	file_content = (char*)malloc( file_len+1 ) ;
	if( file_content == NULL )
	{
		fclose( fp );
		return NULL;
	}
	memset( file_content , 0x00 , file_len+1 );
	
	nret = fread( file_content , 1 , file_len , fp ) ;
	if( nret != file_len )
	{
		fclose( fp );
		free( file_content );
		return NULL;
	}
	
	fclose( fp );
	
	if( p_file_len )
		(*p_file_len) = file_len ;
	return file_content;
}

/* 展开目录路径字符串中的环境变量 */
int ExpandPathFilename( char *pathfilename , long pathfilename_bufsize )
{
	long		pathfilename_len ;
	
	char		*p1 = NULL , *p2 = NULL ;
	char		env_key[ MAXLEN_FILENAME + 1 ] ;
	long		env_key_len ;
	char		*env_val = NULL ;
	long		env_val_len ;
	
	pathfilename_len = strlen(pathfilename) ;
	
	p1 = strchr( pathfilename , '$' );
	while( p1 )
	{
		/* 展开环境变量 */ /* expand environment variable */
		p2 = strchr( p1 + 1 , '$' ) ;
		if( p2 == NULL )
			return RESTSERVER_ERROR_PLACEHOLDER_LACK_OF_CLOSE;
		
		memset( env_key , 0x00 , sizeof(env_key) );
		env_key_len = p2 - p1 + 1 ;
		strncpy( env_key , p1 + 1 , env_key_len - 2 );
		env_val = getenv( env_key ) ;
		if( env_val == NULL )
			return RESTSERVER_FATAL_ENV_VAR_NOT_FOUND;
		
		env_val_len = strlen(env_val) ;
		if( pathfilename_len + ( env_val_len - env_key_len ) > pathfilename_bufsize-1 )
			return RESTSERVER_ERROR_BUFFER_OVERFLOW;
		
		memmove( p2+1 + ( env_val_len - env_key_len ) , p2+1 , strlen(p2+1) + 1 );
		memcpy( p1 , env_val , env_val_len );
		pathfilename_len += env_val_len - env_key_len ;
		
		p1 = strchr( p1 + ( env_val_len - env_key_len ) , '$' );
	}
	
	return 0;
}

