package xyz.calvinwilliams;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class TestAgainstRestServer extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public void init() throws ServletException
	{
		;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter() ;
		out.println("Against restserv");
	}
	
	public void destroy()
	{
		;
	}
}
