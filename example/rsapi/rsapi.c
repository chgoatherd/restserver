/*
 * restserver - A smart, efficient, low consumption RESTful application service platform writen in C
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the Apache License v2.0, see the file LICENSE in base directory.
 */

#include "restserver_api.h"

funcRestServiceEntry GET_hello;
int GET_hello( struct RestServerContext *ctx )
{
	char		response[4096] ;
	int		response_len ;
	
	char		*method = NULL ;
	int		method_len ;
	char		*uri = NULL ;
	int		uri_len ;
	
	int		uri_paths_count ;
	int		uri_path_index ;
	char		*uri_path = NULL ;
	int		uri_path_len ;
	
	int		queries_count ;
	int		query_index ;
	char		*key = NULL ;
	int		key_len ;
	char		*value = NULL ;
	int		value_len ;
	
	int		nret = 0 ;
	
	/* 初始化临时缓冲区 */
	memset( response , 0x00 , sizeof(response) );
	response_len = 0 ;
	
	/* 获取HTTP方法 */
	method = RSAPIGetHttpMethodPtr( ctx , & method_len ) ;
	BUFPRINTF( response , response_len , "method[%.*s]\n" , method_len,method )
	
	/* 获取HTTP路径 */
	uri = RSAPIGetHttpUriPtr( ctx , & uri_len ) ;
	BUFPRINTF( response , response_len , "uri[%.*s]\n" , uri_len,uri )
	
	/* 获取已分解后的路径段 */
	uri_paths_count = RSAPIGetHttpUriPathsCount( ctx ) ;
	BUFPRINTF( response , response_len , "uri_paths_count[%d]\n" , uri_paths_count ) ;
	for( uri_path_index = 1 ; uri_path_index <= uri_paths_count ; uri_path_index++ )
	{
		uri_path = RSAPIGetHttpUriPathPtr( ctx , uri_path_index , & uri_path_len ) ;
		BUFPRINTF( response , response_len , "uri_path[%.*s]\n" , uri_path_len,uri_path )
	}
	
	/* 获取已分解后的参数段 */
	queries_count = RSAPIGetHttpUriQueriesCount( ctx ) ;
	BUFPRINTF( response , response_len , "queries_count[%d]\n" , queries_count ) ;
	for( query_index = 1 ; query_index <= queries_count ; query_index++ )
	{
		key = RSAPIGetHttpUriQueryKeyPtr( ctx , query_index , & key_len ) ;
		value = RSAPIGetHttpUriQueryValuePtr( ctx , query_index , & value_len ) ;
		BUFPRINTF( response , response_len , "query[%d][%.*s][%.*s]\n" , query_index , key_len,key , value_len,value ) ;
	}
	
	/* 构造HTTP缓冲区 */
	nret = RSAPIFormatHttpResponse( ctx , response , response_len , NULL ) ;
	if( nret )
		return nret;
	
	return 0;
}

/* RESTful服务控制器 服务配置区 */
static struct RestServiceConfig		g_rest_services_config[] = {
		{ "GET" , "/rsapi" , GET_hello } ,
		{ "" , "" , NULL }
	} ;

funcInitRestApplication InitRestApplication;
int InitRestApplication( struct RestServerContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	/* 创建RESTful服务控制器 */
	ctl = RSAPICreateRestServiceControler( g_rest_services_config ) ;
	if( ctl == NULL )
		return RESTSERVER_FATAL_CREATE_RESTSERVICECONTROLER;
	
	/* 设置RESTful服务控制器到restserver平台上下文环境中 */
	RSAPISetUserData( ctx , ctl );
	
	return 0;
}

funcCallRestApplication CallRestApplication;
int CallRestApplication( struct RestServerContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	int				nret = 0 ;
	
	/* 从restserver平台上下文环境中取出RESTful服务控制器 */
	ctl = RSAPIGetUserData( ctx ) ;
	if( ctl == NULL )
		return RESTSERVER_FATAL_GET_RESTSERVICECONTROLER;
	
	/* 让RESTful服务控制器分派服务处理入口 */
	nret = RSAPIDispatchRestServiceControler( ctl , ctx ) ;
	if( nret )
		return nret;
	
	return 0;
}

funcCleanRestApplication CleanRestApplication;
int CleanRestApplication( struct RestServerContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	/* 从restserver平台上下文环境中取出RESTful服务控制器 */
	ctl = RSAPIGetUserData( ctx ) ;
	
	/* 销毁RESTful服务控制器 */
	RSAPIDestroyRestServiceControler( ctl );
	
	return 0;
}

