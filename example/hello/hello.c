/*
 * restserver - A smart, efficient, low consumption RESTful application service platform writen in C
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the Apache License v2.0, see the file LICENSE in base directory.
 */

#include "restserver_api.h"

funcRestServiceEntry GET_hello;
int GET_hello( struct RestServerContext *ctx )
{
	char		response[1024] ;
	int		response_len ;
	
	int		nret = 0 ;
	
	/* 初始化临时缓冲区 */
	memset( response , 0x00 , sizeof(response) );
	response_len = 0 ;
	
	/* 填充hello信息 */
	response_len = snprintf( response , sizeof(response) , "Hello restserver\n" ) ;
	
	/* 构造HTTP缓冲区 */
	nret = RSAPIFormatHttpResponse( ctx , response , response_len , NULL ) ;
	if( nret )
		return nret;
	
	return 0;
}

static struct RestServiceConfig		g_rest_services_config[] = {
		{ "GET" , "/hello" , GET_hello } ,
		{ "" , "" , NULL }
	} ;

funcInitRestApplication InitRestApplication;
int InitRestApplication( struct RestServerContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	/* 创建RESTful服务控制器 */
	ctl = RSAPICreateRestServiceControler( g_rest_services_config ) ;
	if( ctl == NULL )
		return RESTSERVER_FATAL_CREATE_RESTSERVICECONTROLER;
	
	/* 设置RESTful服务控制器到restserver平台上下文环境中 */
	RSAPISetUserData( ctx , ctl );
	
	return 0;
}

#include "LOGC.h"

funcCallRestApplication CallRestApplication;
int CallRestApplication( struct RestServerContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	int				nret = 0 ;
	
	/* 从restserver平台上下文环境中取出RESTful服务控制器 */
	ctl = RSAPIGetUserData( ctx ) ;
	if( ctl == NULL )
		return RESTSERVER_FATAL_GET_RESTSERVICECONTROLER;
	
	/* 让RESTful服务控制器分派服务处理入口 */
	nret = RSAPIDispatchRestServiceControler( ctl , ctx ) ;
	if( nret )
		return nret;
	
	return 0;
}

funcCleanRestApplication CleanRestApplication;
int CleanRestApplication( struct RestServerContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	/* 从restserver平台上下文环境中取出RESTful服务控制器 */
	ctl = RSAPIGetUserData( ctx ) ;
	
	/* 销毁RESTful服务控制器 */
	RSAPIDestroyRestServiceControler( ctl );
	
	return 0;
}

