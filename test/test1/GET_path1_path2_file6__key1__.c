/*
 * restserver - A smart, efficient, low consumption RESTful application service platform writen in C
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the Apache License v2.0, see the file LICENSE in base directory.
 */

#include "test.h"

funcRestServiceEntry GET_path1_path2_file6__key1__;
int GET_path1_path2_file6__key1__( struct RestServerContext *ctx )
{
	char		response[4096] ;
	int		response_len ;
	
	int		uri_paths_count ;
	char		*uri_path = NULL ;
	int		uri_path_len ;
	
	int		queries_count ;
	char		*key = NULL ;
	int		key_len ;
	char		*value = NULL ;
	int		value_len ;
	
	int		nret = 0 ;
	
	/* 初始化响应报文 */
	memset( response , 0x00 , sizeof(response) );
	response_len = 0 ;
	
	/* 公共检查 */
	TravelUriPathsAndQueries( ctx , response , sizeof(response) , & response_len );
	
	/* 私有检查 */
	response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "--- REST PRICATE CHECK ---\n" ) ;
	
	/* 检查路径数 */
	uri_paths_count = RSAPIGetHttpUriPathsCount( ctx ) ;
	if( uri_paths_count == 3 )
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check uri_paths_count[%d] ok\n" , uri_paths_count ) ;
	}
	else
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check uri_paths_count[%d] failed\n" , uri_paths_count ) ;
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "--------------------------- CHECK RESULT : FAILURE ---\n" ) ;
		goto _GOTO_GO_RESPONSE;
	}
	
	/* 检查路径值 */
	uri_path = RSAPIGetHttpUriPathPtr( ctx , 1 , & uri_path_len ) ;
	if( STRNCMPRSTR( uri_path , uri_path_len , == , "path1" ) )
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check uri_path ok\n" ) ;
	}
	else
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check uri_path failed\n" ) ;
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "--------------------------- CHECK RESULT : FAILURE ---\n" ) ;
		goto _GOTO_GO_RESPONSE;
	}
	
	uri_path = RSAPIGetHttpUriPathPtr( ctx , 2 , & uri_path_len ) ;
	if( STRNCMPRSTR( uri_path , uri_path_len , == , "path2" ) )
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check uri_path ok\n" ) ;
	}
	else
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check uri_path failed\n" ) ;
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "--------------------------- CHECK RESULT : FAILURE ---\n" ) ;
		goto _GOTO_GO_RESPONSE;
	}
	
	uri_path = RSAPIGetHttpUriPathPtr( ctx , 3 , & uri_path_len ) ;
	if( STRNCMPRSTR( uri_path , uri_path_len , == , "file6" ) )
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check uri_path ok\n" ) ;
	}
	else
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check uri_path failed\n" ) ;
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "--------------------------- CHECK RESULT : FAILURE ---\n" ) ;
		goto _GOTO_GO_RESPONSE;
	}
	
	/* 检查参数数 */
	queries_count = RSAPIGetHttpUriQueriesCount( ctx ) ;
	if( queries_count == 1 )
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check queries_count[%d] ok\n" , queries_count ) ;
	}
	else
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check queries_count[%d] failed\n" , queries_count ) ;
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "--------------------------- CHECK RESULT : FAILURE ---\n" ) ;
		goto _GOTO_GO_RESPONSE;
	}
	
	/* 检查参数值 */
	key = RSAPIGetHttpUriQueryKeyPtr( ctx , 1 , & key_len ) ;
	value = RSAPIGetHttpUriQueryValuePtr( ctx , 1 , & value_len ) ;
	if( STRNCMPRSTR( key , key_len , == , "key1" ) && value && value_len == 0 )
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check query key-value ok\n" ) ;
	}
	else
	{
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "check query key-value failed\n" ) ;
		response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "--------------------------- CHECK RESULT : FAILURE ---\n" ) ;
		goto _GOTO_GO_RESPONSE;
	}
	
	/* 测试结论 */
	response_len += snprintf( response+response_len , sizeof(response)-response_len-1 , "--------------------------- CHECK RESULT : SUCCESS ---\n" ) ;
	
_GOTO_GO_RESPONSE :
	
	/* 设置响应报文 */
	nret = RSAPIFormatHttpResponse( ctx , response , response_len , NULL ) ;
	if( nret )
	{
		ERRORLOG( "RSAPIFormatHttpResponse failed[%d] , errno[%d]" , nret , errno )
		return nret;
	}
	else
	{
		INFOLOG( "RSAPIFormatHttpResponse ok" )
	}
	
	return 0;
}

