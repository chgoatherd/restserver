/*
 * restserver - A smart, efficient, low consumption RESTful application service platform writen in C
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the Apache License v2.0, see the file LICENSE in base directory.
 */

#include "test.h"

static struct RestServiceConfig		g_rest_services_config[] = {
		{ "GET" , "/" , GET_ } ,					/* curl "http://192.168.6.74:7911/" */
		{ "GET" , "/path1" , GET_path1 } ,				/* curl "http://192.168.6.74:7911/path1" */
		{ "GET" , "/path1/" , GET_path1_ } ,				/* curl "http://192.168.6.74:7911/path1/" */
		{ "GET" , "/path1/path2" , GET_path1_path2 } ,		/* curl "http://192.168.6.74:7911/path1/path2" */
		{ "GET" , "/path1/path2/" , GET_path1_path2_ } ,		/* curl "http://192.168.6.74:7911/path1/path2/" */
		{ "GET" , "/path1/path2/file" , GET_path1_path2_file } ,	/* curl "http://192.168.6.74:7911/path1/path2/file" */
		{ "GET" , "/path1/{}/file" , GET_path1_n_file } ,		/* curl "http://192.168.6.74:7911/path1/123/file1" */
		{ "GET" , "/path1/path2/file1" , GET_path1_path2_file1__key1_value1 } ,		/* curl "http://192.168.6.74:7911/path1/path2/file1?key1=value1" */
		{ "GET" , "/path1/path2/file2" , GET_path1_path2_file2__key1_value1__key2_value2 } ,	/* curl "http://192.168.6.74:7911/path1/path2/file2?key1=value1&key2=value2" */
		{ "GET" , "/path1/path2/file3" , GET_path1_path2_file3__ } ,				/* curl "http://192.168.6.74:7911/path1/path2/file3?" */
		{ "GET" , "/path1/path2/file4" , GET_path1_path2_file4__key1 } ,			/* curl "http://192.168.6.74:7911/path1/path2/file4?key1" */
		{ "GET" , "/path1/path2/file5" , GET_path1_path2_file5__key1_ } ,			/* curl "http://192.168.6.74:7911/path1/path2/file5?key1=" */
		{ "GET" , "/path1/path2/file6" , GET_path1_path2_file6__key1__ } ,			/* curl "http://192.168.6.74:7911/path1/path2/file6?key1&" */
		{ "GET" , "/path1/path2/file7" , GET_path1_path2_file7__key1___ } ,			/* curl "http://192.168.6.74:7911/path1/path2/file7?key1=&" */
		{ "POST" , "/path1/file" , POST_path1_file } ,		/* curl -X POST -d "this is a POST test for restserver" "http://192.168.6.74:7911/path1/file" */
		{ "PUT" , "/path1/file" , PUT_path1_file } ,		/* curl -X PUT -d "this is a PUT test for restserver" "http://192.168.6.74:7911/path1/file" */
		{ "DELETE" , "/path1/file" , DELETE_path1_file } ,	/* curl -X DELETE -d "this is a DELETE test for restserver" "http://192.168.6.74:7911/path1/file" */
		{ "" , "" , NULL }
	} ;

funcInitRestApplication InitRestApplication;
int InitRestApplication( struct RestServerContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	ctl = RSAPICreateRestServiceControler( g_rest_services_config ) ;
	if( ctl == NULL )
	{
		ERRORLOG( "RSAPICreateRestServiceControler failed" )
		return RESTSERVER_FATAL_CREATE_RESTSERVICECONTROLER;
	}
	else
	{
		INFOLOG( "RSAPICreateRestServiceControler ok" )
	}
	
	RSAPISetUserData( ctx , ctl );
	INFOLOG( "RSAPISetUserData ctl[%p]" , ctl )
	
	return 0;
}

funcCallRestApplication CallRestApplication;
int CallRestApplication( struct RestServerContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	int				nret = 0 ;
	
	ctl = RSAPIGetUserData( ctx ) ;
	INFOLOG( "RSAPIGetUserData ctl[%p]" , ctl )
	if( ctl == NULL )
		return RESTSERVER_FATAL_GET_RESTSERVICECONTROLER;
	
	nret = RSAPIDispatchRestServiceControler( ctl , ctx ) ;
	if( nret )
	{
		ERRORLOG( "RSAPIDispatchRestServiceControler failed[%d]" , nret )
		return nret;
	}
	else
	{
		INFOLOG( "RSAPIDispatchRestServiceControler ok" )
	}
	
	return 0;
}

funcCleanRestApplication CleanRestApplication;
int CleanRestApplication( struct RestServerContext *ctx )
{
	struct RestServiceControler	*ctl = NULL ;
	
	ctl = RSAPIGetUserData( ctx ) ;
	INFOLOG( "RSAPIGetUserData ctl[%p]" , ctl )
	
	RSAPIDestroyRestServiceControler( ctl );
	INFOLOG( "RSAPIDestroyRestServiceControler ctl[%p]" , ctl )
	
	return 0;
}

